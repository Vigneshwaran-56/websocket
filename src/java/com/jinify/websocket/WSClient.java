/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jinify.websocket;

import java.net.URI;
import java.util.logging.Logger;
import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import org.json.JSONObject;

/**
 *
 * @author tringapps
 */
@ClientEndpoint
public class WSClient {
    
     WebSocketContainer container = null;
        Session session = null;
    @OnMessage
    public String onmessage(String message){
        System.out.println("i rcvd this "+message);
        Logger.getLogger("check").warning("i rcvd this "+message);
                
        return null;
    }
    public void execute(JSONObject msg) {
    try {
            container = ContainerProvider. getWebSocketContainer();
            session = container.connectToServer(this, URI.create("ws://localhost:8084/jinify-websocket/wsEndpint"));
            
            session.getBasicRemote().sendText(msg.toString());
            


        } catch (Exception e) {
            Logger.getLogger("check").warning(" Error on Execute method "+e.getMessage());
            e.printStackTrace();

        } 
    }
}
