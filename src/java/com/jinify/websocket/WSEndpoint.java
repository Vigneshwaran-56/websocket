/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jinify.websocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author tringapps
 */
@ServerEndpoint("/wsEndpint")
public class WSEndpoint {
    
     private static Set<Session> session = Collections.synchronizedSet(new HashSet<Session>());
  
   private static Map<String,Collection<Session>> sessionUserMap= new HashMap<String,Collection<Session>>();
  
   @OnOpen
   public void onOpen(Session userSession ){
        System.out.println(userSession.getId() + " has opened a connection"); 
        Logger.getLogger("check").warning(userSession.getId() + "has opened a connection");
        
        session.add(userSession);
    }
   
    /**
     *
     * @param userSession
     */
    @OnMessage
   public void onMessage(String message,Session userSession) throws IOException, JSONException{
       
     
           Logger.getLogger("check").warning(message+ "received");
          System.out.println( message.contains("remote"));
           if(message.contains("remote")){
        	   
        	 int indexof = message.indexOf(":");
        	 
          Collection<Session> collectionSession = new ArrayList<Session>();  
          collectionSession.add(userSession);
               System.out.println("usermail"+message.substring(0, indexof));
          sessionUserMap.put(message.substring(0, indexof), collectionSession);
               try {
                   userSession.getBasicRemote().sendText("Connection Establisheed");
               } catch (IOException ex) {
                   Logger.getLogger(WSEndpoint.class.getName()).log(Level.SEVERE, null, ex);
               }
        	 
           
           }else if(message.contains("client")){
        	   
					JSONObject json = new JSONObject(message);
           Logger.getLogger("check").warning(json+ "json");

					String usermail = (String) json.get("usermail");

					int indexof = usermail.indexOf(":");

					usermail = usermail.substring(0, indexof);
                               System.out.println(usermail);

			System.out.println(sessionUserMap.keySet());
        	   if(sessionUserMap.containsKey(usermail)){
        		   
        		   Collection<Session> collectionSession = sessionUserMap.get(usermail);
        		   for(Session userAgentSession:collectionSession){
        		   userAgentSession.getBasicRemote().sendText(message);
                                      Logger.getLogger("check").warning(json+ "message");

                           }
                           
        	   }else{
              	 userSession.getBasicRemote().sendText("Connection Failed");

        	   }
        	   
        	   
           }
   }
   
   @OnClose
    public void onClose(Session userSession){
        System.out.println("Session " +userSession.getId()+" has ended");
        Logger.getLogger("check").warning("Session " +userSession.getId()+" has ended");
        session.remove(session);
              
    }
    
    
}
