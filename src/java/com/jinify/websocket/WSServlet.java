package com.jinify.websocket;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;



/**
 * Servlet implementation class WSServlet
 */
@WebServlet("/wsServlet")
public class WSServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
		
response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		JSONObject jsonobject = new JSONObject(request.getParameter("json"));
                

		jsonobject.put("usermail", request.getParameter("usermail")+":client");

		response.getWriter().append("Served at: ").append(jsonobject+"");

	WSClient  wsClient = new WSClient();
        wsClient.execute(jsonobject);
        
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            try {
                processRequest(request, response);
            } catch (JSONException ex) {
                Logger.getLogger(WSServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            try {
                processRequest(request, response);
            } catch (JSONException ex) {
                Logger.getLogger(WSServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    

	}

